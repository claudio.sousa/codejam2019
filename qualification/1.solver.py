def run():
    T = int(input())
    case = 1
    for _ in range(T):
        str_in = input().rstrip()
        nb1 = ''.join([c if c != '4' else '2' for c in str_in])
        nb2 = ''.join(['0' if c != '4' else '2' for c in str_in])

        print("Case #{}: {} {}".format(case, nb1, nb2))
        case += 1



if __name__ == '__main__':
    run()
