def run():
    T = int(input())
    case = 1
    for _ in range(T):
        N = int(input())-1
        path = input().rstrip()
        res = ''.join(['E' if c == 'S' else 'S' for c in path])
        print("Case #{}: {}".format(case, res))
        case += 1



if __name__ == '__main__':
    run()
