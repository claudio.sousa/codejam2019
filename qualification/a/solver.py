dirs = {
    'N': (0, 1),
    'S': (0, -1),
    'W': (-1, 0),
    'E': (1, 0)
}
def run():
    T = int(input())
    for t in range(T):
        P, Q = map(int, input().split())
        grid = [[0] * (Q + 1) for _ in range(Q + 1)]
        persons = []
        for p in range(P):
            inputs = input().split()
            x, y, d = int(inputs[0]), int(inputs[1]), dirs[inputs[2]]
            persons.append((x, y, d))

        best = 0
        bests_pos = (0, 0)

        for i, p1 in enumerate(persons):
            for j, p2 in enumerate(persons):



        while True:
            x += d[0]
            y += d[1]
            if not (0 <= x <= Q and 0 <= y <= Q):
                break
            if d[0] != 0:
                for y1 in range(Q+1):
                    grid[x][y1] += 1
            else:
                for x1 in range(Q+1):
                    grid[x1][y] += 1

        best = -1
        best_pos = (0, 0)
        for x in range(Q + 1):
            for y in range(Q + 1):
                if grid[x][y] > best:
                    best = grid[x][y]
                    best_pos = (x, y)
        print("Case #{}: {} {}".format(t + 1, *best_pos))


if __name__ == '__main__':
    run()
