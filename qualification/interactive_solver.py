import sys
from itertools import permutations
from copy import deepcopy
from random import choices


def run():
    T, F = map(int, raw_input().split())
    sys.stderr.write("{} {}\n".format(T, F))

    for _ in range(T):
        possible = permutations(['A', "B", "C", "D", "E", "F"])
        for i in range(120):
            pos_possible = deepcopy(possible)
            nbs = choices(range(5), k=4)
            res = [None] * 5
            for nb in nbs:
                print(str(1+i*5+nb))
                sys.stdout.flush()
                inp = raw_input()
                res[nb] = inp

            sys.stderr.write("{}\n".format(inp))
        print("ABCED")
        sys.stdout.flush()
        res = raw_input()
        sys.stderr.write(res)
        if res == 'N':
            break

    exit()


if __name__ == '__main__':
    run()
